__author__ = 'Adriano'


class Checkout(object):

    def __init__(self, rules):
        self.rules = rules
        self.itens = []

    def scan(self, item):
        if item != "":
            self.itens.append(item.upper())

    def calculate_total(self):
        self.itens.sort()
        previous_item = None
        total = 0
        for item in self.itens:
            if item != previous_item:
                previous_item = item
                rule = self.rules.get(item).split('|')
                unit_price = float(rule[0])
                promotional_quantity = int(rule[1])
                promotional_price = float(rule[2])
                total_items = int(self.itens.count(item))
                if promotional_quantity == 0:
                    total += unit_price*total_items
                else:
                    total += (int((total_items/promotional_quantity))*promotional_price) + ((total_items % promotional_quantity)*unit_price)
        return total

    def get_rules(self):
        return self.rules

if __name__ == '__main__':
    rules = {}
    all_products = input("Digite todos os produtos que vc deseja cadastrar (separados por - )")
    products = all_products.upper().split('-')
    for product in products:
        unit_price = input("Digite o preco da unidade do produto %s:  " % product)
        promotional_quantity = input("Digite a quantidade promocional do produto %s:  " % product)
        if promotional_quantity != "" and promotional_quantity != 0:
            promotional_price = input("Digite o preco promocional do produto %s  " % product)
        else:
            promotional_quantity = "0"
            promotional_price = "0"
        rules.update({product: unit_price + "|" + promotional_quantity + "|" + promotional_price})
    checkout = Checkout(rules)
    print("")
    print("### Scanner de Itens ###")
    print("")
    continue_loop = True
    while continue_loop:
        item = input("Digite o nome do item que deseja adicionarao carrinho: (digite XXX para calcular o total)  ")
        if item.upper() == "XXX":
            continue_loop = False
        elif item.upper() in checkout.get_rules():
            checkout.scan(item)
        else:
            print("Item inexistente, tente novamente")
    total = checkout.calculate_total()
    print("### TOTAL DE COMPRAS: " + str(total))




