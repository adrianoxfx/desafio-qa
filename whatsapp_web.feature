# language: pt

Funcionalidade: Como usuário quero acessar o whatsapp pelo browser
  O sistema deve permitir a conexão do browser com o aplicativo do smartphone
  Não é permitido conectar através de um browser mobile


  Cenario: Conectar um browser para desktop ao aplicativo no smartphone
    Dado que estou em um browser para desktop
    Quando acesso "web.whatsapp.com"
    E clico em "WhatsApp Web" no aplicativo
    E clico em "+"
    E escaneio o QR Code do browser com o smartphone
    Então conecto o browser ao aplicativo

  Cenario: Não conectar um browser mobile ao aplicativo no smartphone
    Dado que estou em um browser mobile
    Quando acesso "web.whatsapp.com"
    Então o QR Code não é exibido no browser

