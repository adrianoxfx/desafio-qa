# language: pt

Funcionalidade: Como usuário quero silenciar uma conversa
  O sistema deve permitir o bloqueio das notificações de alguma conversa pelo periodo de 8 horas, 1 semana ou 1 ano

  Cenario: Silenciar a conversa de um contato por 8 horas
    Dado que eu tenha 1 contato no WhatsApp
    Quando eu clico em "Conversas"
    E seleciono a conversa com o contato
    E clico em "Silenciar"
    E clico em "8 Horas"
    Então não recebo notificações do contato por "8 horas"

  Cenario: Silenciar a conversa de um grupo por 1 ano
    Dado que eu tenha 1 grupo no WhatsApp
    Quando eu clico em "Conversas"
    E seleciono a conversa com o grupo
    E clico em "Silenciar"
    E clico em "1 Ano"
    Então não recebo notificações do grupo por "1 Ano"
