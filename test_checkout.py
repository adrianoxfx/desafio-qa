import unittest
from checkout import Checkout

__author__ = 'Adriano'



class TestCheckout(unittest.TestCase):

    def tearDown(self):
        self.checkout = None

    def price(self, goods):

        rules = ({'A': '50|3|130', 'B': '30|2|45', 'C': '20|0|0', 'D': '15|0|0'})
        self.checkout = Checkout(rules)

        itens = goods.split('-')

        for item in itens:
            self.checkout.scan(item)

        return self.checkout.calculate_total()

    def test_totals(self):
        self.assertEqual(0, self.price(""))
        self.assertEqual(50, self.price("A"))
        self.assertEqual(80, self.price("A-B"))
        self.assertEqual(115, self.price("C-D-B-A"))

        self.assertEqual(100, self.price("A-A"))
        self.assertEqual(130, self.price("A-A-A"))
        self.assertEqual(180, self.price("A-A-A-A"))
        self.assertEqual(230, self.price("A-A-A-A-A"))
        self.assertEqual(260, self.price("A-A-A-A-A-A"))

        self.assertEqual(160, self.price("A-A-A-B"))
        self.assertEqual(175, self.price("A-A-A-B-B"))
        self.assertEqual(190, self.price("A-A-A-B-B-D"))
        self.assertEqual(190, self.price("D-A-B-A-B-A"))

    def test_incremental(self):
        rules = ({'A': '50|3|130', 'B': '30|2|45', 'C': '20|0|0', 'D': '15|0|0'})
        self.checkout = Checkout(rules)

        self.assertEqual(0, self.checkout.calculate_total())
        self.checkout.scan("A")
        self.assertEqual(50, self.checkout.calculate_total())
        self.checkout.scan("B")
        self.assertEqual(80, self.checkout.calculate_total())
        self.checkout.scan("A")
        self.assertEqual(130, self.checkout.calculate_total())
        self.checkout.scan("A")
        self.assertEqual(160, self.checkout.calculate_total())
        self.checkout.scan("B")
        self.assertEqual(175, self.checkout.calculate_total())


if __name__ == '__main__':
    unittest.main()